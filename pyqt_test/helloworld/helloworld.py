# -*- coding: utf-8 -*-
import PyQt4, PyQt4.QtGui, sys

"""
Module implementing DialogHelloworld.
"""

from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignature

from Ui_helloworld import Ui_Dialog

class DialogHelloworld(QDialog, Ui_Dialog):
    """
    Class documentation goes here.
    """
    def __init__(self, parent = None):
        """
        Constructor
        """
        QDialog.__init__(self, parent)
        self.setupUi(self)
    
    @pyqtSignature("")
    def on_OK_clicked(self):
        """
        Slot documentation goes here.
        """
        # TODO: not implemented yet
        self.label.setText("Test2")

if __name__ == "__main__":
    app = PyQt4.QtGui.QApplication(sys.argv)
    dlg = DialogHelloworld()
    dlg.show()
    sys.exit(app.exec_())
